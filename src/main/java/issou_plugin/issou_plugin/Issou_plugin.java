package issou_plugin.issou_plugin;

import issou_plugin.issou_plugin.commands.rp;
import issou_plugin.issou_plugin.events.onEntityDamage;
import issou_plugin.issou_plugin.events.onProjectile;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class Issou_plugin extends JavaPlugin {

    public static void issouSound(Location l ){
        for(Player pl: Bukkit.getOnlinePlayers()){
            pl.playSound(l, "sounds.issou", 1, 1);
        }
    }

    @Override
    public void onDisable() {
        // Call on shutdown
    }

    @Override
    public void onEnable() {
        // Call on boot
        getServer().getPluginManager().registerEvents(new onProjectile(), this);
        getServer().getPluginManager().registerEvents(new onEntityDamage(), this);
        getCommand("rp").setExecutor(new rp());
    }
}
