package issou_plugin.issou_plugin.events;

import issou_plugin.issou_plugin.Issou_plugin;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;

/**
 * Created by sonic on 22/05/2018.
 * Copyright hugo_faurand©
 */
public class onProjectile implements Listener {
    @EventHandler
    public void onThrow (ProjectileLaunchEvent event){
        Entity e = event.getEntity();
        if(e.getType() == EntityType.SNOWBALL){
            if(((Projectile) e).getShooter() instanceof  Player){
                Player p = (Player) ((Projectile) e).getShooter();
                Issou_plugin.issouSound(p.getLocation());
            }

        }
    }
}
