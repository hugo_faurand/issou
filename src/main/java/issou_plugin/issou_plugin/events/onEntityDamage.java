package issou_plugin.issou_plugin.events;

import issou_plugin.issou_plugin.Issou_plugin;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by sonic on 26/05/2018.
 * Copyright hugo_faurand©
 */
public class onEntityDamage implements Listener {
    @EventHandler
    public void entityDamage(EntityDamageByEntityEvent event)
    {

        Entity entity = event.getEntity();
        Entity damager = event.getDamager();

        if(damager instanceof Snowball) {
            Snowball snowball = (Snowball) damager;

            if(snowball.getShooter() instanceof Snowman) {

                Location location = entity.getLocation();
                World w = Bukkit.getWorld(location.getWorld().getName());
                Issou_plugin.issouSound(location);

                if(entity instanceof Snowman)
                {
                    w.spawnParticle(Particle.HEART,location,10);

                }else {

                    entity.remove();
                    w.spawnEntity(location, EntityType.SNOWMAN);
                    w.spawnParticle(Particle.EXPLOSION_LARGE, location, 4);
                    w.playSound(location,Sound.ENTITY_FIREWORK_TWINKLE,1.0F,1.0F);
                }
            }
        }
        if(damager instanceof Egg)
        {
            Egg egg = (Egg) damager;
            if(egg.getShooter() instanceof Player)
            {

                Location location = entity.getLocation();
                World world =Bukkit.getWorld(location.getWorld().getName());
                entity.setCustomNameVisible(true);
                Issou_plugin.issouSound(location);
                entity.setCustomName(ChatColor.RED+"RisiTas"+ChatColor.BOLD+" Au "+ChatColor.BLUE+"RsA");
            }
        }

    }
    @EventHandler
    public void EntityDamageEvent( EntityDamageEvent damageEvent)
    {

        if(damageEvent.getEntity() instanceof Snowman && damageEvent.getCause() == EntityDamageEvent.DamageCause.DROWNING)
        {
            damageEvent.setCancelled(true);
        }

    }

}
